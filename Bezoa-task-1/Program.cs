﻿using System;
using Bezoatask1.Services;

namespace Bezoatask1
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.Write("Enter your name: ");
            var yourName = Console.ReadLine();
            Console.Write("Enter your date of birth: format(YYYY/MM/DD) ");
            string dob = Console.ReadLine();
            int age = AgeCalculator.CalculateAge(dob);
            AgeCalculator.PrintInfo(yourName, age);
        }
    }
}
