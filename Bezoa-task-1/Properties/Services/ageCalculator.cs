﻿using System;

namespace Bezoatask1.Services
{
    public static class AgeCalculator
    {
        public static int CalculateAge(string dob)
        {
            DateTime dateOfBirth = Convert.ToDateTime(dob);
            int age = 0;
            age = DateTime.Now.Year - dateOfBirth.Year;
            if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
                age = age - 1;
            return age;
        }
        public static void PrintInfo(string name, int age)
        {
            Console.WriteLine($"Hi {name},you are {age} years old.");
        }
    }
}
